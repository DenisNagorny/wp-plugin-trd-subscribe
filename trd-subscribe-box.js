jQuery(document).ready(function() {
	var block = jQuery("#subscribe-message"), 
		blockWidth = block.outerWidth(),
		need_height = 1/2 * jQuery('body').height(),
		closed = false,
		shown = false,
		email = jQuery('.subscribe-email', block),
		speed = 200,
		scrolledOver = false;
	
	if (jQuery.browser.msie  && parseInt(jQuery.browser.version, 10) === 8) {
		email.css('padding-top', '8px');
		email.height('24px');
	}
	if (jQuery.browser.msie  && parseInt(jQuery.browser.version, 10) === 7) {
		email.css('padding-top', '8px');
		email.height('22px');
		email.css('vertical-align', 'middle');
	}
	
	email.val('Email');
	email.live('focus', function() {
		if (this.value == 'Email') {this.value = '';}
		var trim_val = jQuery.trim(this.value);
		this.value = trim_val;
		
	});
	
	email.live('blur', function() {
		if (this.value == '') {this.value = 'Email';}
		var trim_val = jQuery.trim(this.value);
		this.value = trim_val;
		if (this.value == '') {this.value = 'Email';}
	});
	
	
	block.css('right', '-' + blockWidth + 'px');

	function getCookie(c_name) {
		var i,x,y,ARRcookies=document.cookie.split(";");
		for (i=0;i<ARRcookies.length;i++) {
			x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
			y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
			x=x.replace(/^\s+|\s+$/g,"");
			if (x==c_name) {
				return unescape(y);
			}
		  }
	}
	//var trd_cookie = getCookie('trd');
	//console.log('Cookie: ' + trd_cookie);
	
	jQuery(window).scroll(function() {
	var cur = jQuery(window).scrollTop() + 3/4*jQuery(window).height();
	if (!shown && !closed && cur > need_height) {
			if (!scrolledOver) {
				block.show();
				block.animate({right: 0}, speed);
				shown = true;
				scrolledOver = true; 
			} 
		} else{
			scrolledOver = false;
		}
	});

	
	if (!shown && !closed && jQuery('body').height() <= jQuery(window).height()) {
		block.show();
		block.delay(2000).animate({right: 0}, speed);
		shown = true;
		
	}
	
	jQuery(".submit-image", block).click(function(event) {
		block.hide();
	});
	
	
	
	

	jQuery(".close_button", block).click(function(event) {
		event.preventDefault(event);

		var ws=new Date();
		ws.setDate(7 + ws.getDate());
		document.cookie="trd=yes; path=/; expires="+ ws.toGMTString();
		
		block.animate({right: '-' + blockWidth + 'px'}, speed);
		closed = true;
	});
	
});