<?php 
/*
Plugin Name: TRD subscribe
Plugin URI: http://soundst.com
Description: This plugin adds slideout subsctiption box in the lower right part of the browser window when user scrolls 2/3 of a page.
Author: Denis Nagorny
Version: 0.1.6
*/
if (!isset($_COOKIE['trd'])) {
	add_action( 'wp_enqueue_scripts', 'add_subscribe_style' );
	add_action('init', 'add_subscribe_box');
	add_action('wp_footer', 'add_content_box');
}

function add_subscribe_style() {
	wp_register_style( 'subscribe-style', plugins_url('trd-subscribe-box.css', __FILE__) );
	wp_enqueue_style( 'subscribe-style' );
}

function add_subscribe_box() {
	wp_register_script( 'jquery-subscribe', plugins_url('trd-subscribe-box.js', __FILE__), array('jquery'), '1.1', false );
	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'jquery-subscribe' );
}

function add_content_box() {
	$close_button = plugin_dir_url(__FILE__) . 'img/close_button.png';
	$subscribe_button = plugin_dir_url(__FILE__) . 'img/subscribe_button.png';
	$arrow = plugin_dir_url(__FILE__) . 'img/arrow.png';
	$logo = plugin_dir_url(__FILE__) . 'img/TRD.png';
	$content = "<div id='subscribe-message'><div class='subscribe-margin'>
	<a href='#' class='close_button' title='Close'><img src='{$close_button}' title='Close' alt='Close' /></a>
	<p><img src='{$logo}' /></p>
	<div class='subscribe_arrow'><img src='{$arrow}' /></div>
	<form action='http://visitor.r20.constantcontact.com/d.jsp' method='post' target='_blank'>
	<label for='subscribe'><span>Email</span> (required)</label> <input id='subscribe' class='subscribe-email' type='text' name='ea'>
	<input type='hidden' name='llr' value='rwrbr6iab'>
	<input type='hidden' name='m' value='1109092517585'>
	<input type='hidden' name='p' value='oi'>
	<input valign='baseline' class='submit-image' type='image' name='submit' src='{$subscribe_button}' title='Subscribe' alt='Subscribe'></form></div></div>";
	echo $content;
}
